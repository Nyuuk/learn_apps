/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ["./learn/www/**/*.html", "./learn/templates/**/*.html"],
	theme: {
		extend: {},
	},
	plugins: [],
};
