# Copyright (c) 2024, Nyuuk and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from werkzeug.wrappers import Response
import json
from datetime import datetime
from frappe.sessions import Session


class LearnChatHistory(Document):
    def after_insert(Document):
        frappe.publish_realtime("learn_chat_event", f"<li hx-swap-oob='beforeend:#content'>{Document.sender_id}: {Document.content}</li>")


@frappe.whitelist(allow_guest=True, methods=["GET"])
def get_all(id_chat):
	last_modified = frappe.db.get_value("LearnChat", {"name": id_chat}, "modified")

	# Mendapatkan nama hari
	day_name = last_modified.strftime("%a")

	# Mendefinisikan format tanggal dan waktu
	formatted_date_time = last_modified.strftime("%d %b %Y %H:%M:%S")

	# Menggabungkan semua komponen menjadi format yang diinginkan
	final_formatted_date_time = f"{day_name}, {formatted_date_time} GMT"


	data_messages = frappe.get_all("LearnChatHistory", {'parent': id_chat}, ['sender_id', 'content'])
	data_html = ''

	for data in data_messages:
		data_return = {}
		frappe.response['data'] = data
		data_return['sender_id'] = data['sender_id']
		data_return['message'] = json.loads(data['content'])['content']
		if data['sender_id'] == frappe.session.user:
			data_html += frappe.render_template("templates/components/buble-right.html", {"data": data_return})
		else:
			data_html += frappe.render_template("templates/components/buble-left.html", {"data": data_return})
	data_html += ''
	value_cache = frappe.cache.get_value(f"learn_chat_msg_notification_{frappe.session.user}")
	if frappe.session.user == "Guest" and value_cache is None:
		set_notif("You are not permitted to access this resource. so you are not allowed to send message.", error=True)

	if frappe.cache.get_value(f"learn_chat_msg_notification_{frappe.session.user}"):
		data_html += frappe.render_template("templates/components/notification.html", {"error": value_cache["error"], "message": value_cache["message"]})
	else:
		data_html += frappe.render_template("templates/components/notification.html", {"error": None, "message": ""})


	response = Response(data_html)
	response.headers["cache_data"] = value_cache
	response.headers["user-id"] = frappe.session.user
	# response.headers["Last-Modified"] = final_formatted_date_time
	# response.headers['Cache-Control'] = 'public, max-age=3600'
	# response.headers['Vary'] = 'Accept-Encoding'
	return response

@frappe.whitelist(allow_guest=True, methods=["POST"])
def create_new(msg):
	sender_id = frappe.session.user

	if sender_id == "Guest":
		set_notif("You are not permitted to send message.", error=True)
		return Response("")
	parent = frappe.get_doc('LearnChat', "General")
	# parent.append("history", new_msg)
	new_msg = parent.append("history", {})
	new_msg.sender_id = sender_id
	new_msg.content = {"content": msg}
	parent.save()
	set_notif("Success send message", error=False)
	response = Response("")
	response.headers["cache_data"] = frappe.cache.get_value(f"learn_chat_msg_notification_{frappe.session.user}")
	return response

def set_notif(msg, error=False, expires_in_sec=15):
	frappe.cache.set_value(f"learn_chat_msg_notification_{frappe.session.user}", {"message": msg, "error": error}, expires_in_sec=expires_in_sec)
